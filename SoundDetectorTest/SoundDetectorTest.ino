/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

 This example code is in the public domain.
 */
 
  int Max_Audio = 0;
  int Max_Envelope = 0;
   int count = 0;
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(5, INPUT);
 
}


// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int Audio = analogRead(A0);
  int Envelope = analogRead(A1);
  bool Gate = digitalRead(5);
/*
  if (Audio > Max_Audio)
  {
    Max_Audio = Audio;
  }
  if (Envelope > Max_Envelope)
  {
    Max_Envelope = Envelope;
  }
*/ 
  // print out the value you read:
    //Serial.print("Audio: ");
    Serial.print(Audio);
    Serial.print(",");
    //Serial.print("Envelope: ");
    Serial.print(Envelope);
    //Serial.print("Gate: ");
    //Serial.println(Gate);
    Serial.println();
  /*
    if (count >= 1000)
  {
    Serial.print("Max_Audio: ");
    Serial.println(Max_Audio);
    Serial.print("Max_Envelope: ");
    Serial.println(Max_Envelope);
    Serial.print("Gate: ");
    Serial.println(Gate);
    Serial.println();

    Max_Audio = 0;
    Max_Envelope = 0;
    count = 0;
  }
  
 count++;
 */
}
