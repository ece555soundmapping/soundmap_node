// Include the ESP8266 WiFi library. (Works a lot like the
// Arduino WiFi library.)
#include <ESP8266WiFi.h>
//#include "RunningAverage.h"

////////////////////
// Collect States //
////////////////////
#define STATE_INIT         0
#define STATE_SAMPLE       1
#define STATE_PEAK_PEAK    2
#define STATE_SEND         3

/////////////////
// Send States //
/////////////////
#define SSTATE_INIT         0
#define SSTATE_CONNECT      1
#define SSTATE_SEND         2
#define SSTATE_READY        3
#define SSTATE_INIT_WAIT    4

//////////////////////
// Node Definitions //
//////////////////////
String nodeID = "0";

//////////////////////
// WiFi Definitions //
//////////////////////
const char WiFiSSID[] = "ut-open";
const char WiFiPSK[] = "";

/////////////////////
// Pin Definitions //
/////////////////////
const int LED_PIN = 5; // Thing's onboard, green LED
const int ANALOG_PIN = A0; // The only analog pin on the Thing
const int DIGITAL_PIN = 12; // Digital pin to be read

///////////////
// Host Info //
///////////////
const char Host[] = "50.142.48.24";
//const char PublicKey[] = "wpvZ9pE1qbFJAjaGd3bn";
//const char PrivateKey[] = "wzeB1z0xWNt1YJX27xdg";
const int Port = 5555;

/////////////////
// Post Timing //
/////////////////
const unsigned long postRate = 5000;
unsigned long lastPost = 0;

//////////////////////
// Global Variables //
//////////////////////
//RunningAverage running(0xff);
int sample;
const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
double running_av = 0;
long double total = 0;
unsigned long int n = 0;
int collectState = 0;
int sendState = 0;
float sendValue;
byte ledStatus = LOW;
int cycleCount = 0;
WiFiClient client;

void setup()
{
  initHardware();
  digitalWrite(LED_PIN, HIGH);

  sample = 0;
}

void loop()
{
  // Post the data to the server at post rate
  CollectAndSendData();
  WiFiLoop();
  //delay(100);

  // Update the average sound value
  //updateSoundData();
  //Serial.println(5);
}

void initHardware()
{
  Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  // Don't need to set ANALOG_PIN as input,
  // that's all it can be.

  //Serial.println("D: Init Complete");
}

int SendData(float value)
{
   digitalWrite(LED_PIN, HIGH);
   if (sendState == SSTATE_READY)
   {
      sendValue = value;
      sendState = SSTATE_SEND;
      //  Serial.println("D: SendValue");
   }

  // LED turns on when we enter, it'll go off when we
  // successfully post.
  //digitalWrite(LED_PIN, HIGH);

  //Serial.println("D: Start Post");

  // Before we exit, turn the LED off.
  //digitalWrite(LED_PIN, LOW);

  //Serial.println("D: Successful Post");

  //client.stop();
}

void WiFiLoop()
{
   if (sendState == SSTATE_INIT)
   {
      // Set WiFi mode to station (as opposed to AP or AP_STA)
      WiFi.mode(WIFI_STA);

      // WiFI.begin([ssid], [passkey]) initiates a WiFI connection
      // to the stated [ssid], using the [passkey] as a WPA, WPA2,
      // or WEP passphrase.
      WiFi.begin(WiFiSSID, WiFiPSK);

      // Do a little work to get a unique-ish name. Append the
      // last two bytes of the MAC (HEX'd) to "Thing-":
      //uint8_t mac[WL_MAC_ADDR_LENGTH];
      uint8_t mac[WL_MAC_ADDR_LENGTH];
      WiFi.macAddress(mac);
      String macID;
      for (int i = 0; i<WL_MAC_ADDR_LENGTH; i++)
      {
          macID += String(mac[i], HEX);     
      }
    
      macID.toUpperCase();
      Serial.println(macID);

      sendState = SSTATE_INIT_WAIT;
   }

   else if (sendState == SSTATE_INIT_WAIT)
   {
      // Use the WiFi.status() function to check if the ESP8266
      // is connected to a WiFi network.
      if (WiFi.status() == WL_CONNECTED)
      {
      //   Serial.println("D: WiFi Started");
         sendState = SSTATE_CONNECT;
      }
      else
      {
         // WiFi.printDiag(Serial);
         // Blink the LED
         digitalWrite(LED_PIN, ledStatus); // Write LED high/low
         ledStatus = (ledStatus == HIGH) ? LOW : HIGH;
         delay(100);
      }
   }

   else if (sendState == SSTATE_CONNECT)
   {
      if (WiFi.status() == WL_CONNECTED)
      {
         if (client.connect(Host, Port))
         {
            sendState = SSTATE_READY;
            // Serial.println("D: Client Connected");
         }
      }
      // WiFi not connected
      else
      {
         sendState = SSTATE_INIT;
      }
   }

   else if (sendState == SSTATE_READY)
   {
   }

   else if (sendState == SSTATE_SEND)
   {
      // Check to make sure connection is still good.
      if (WiFi.status() == WL_CONNECTED)
      {
         if (client.connected())
         {
            // Post to the server
            client.print("SS ");
            client.print(nodeID);
            client.print(" ");
            client.print(sendValue);
            client.println();
            sendState = SSTATE_READY;
            digitalWrite(LED_PIN, LOW);
            // Serial.println("D: Send Complete");
         }
         // Client not connected
         else
         {
            sendState = SSTATE_CONNECT;
         }
      }
      // WiFi not connected
      else
      {
         sendState = SSTATE_INIT;
      }
   }

   else
   {
      sendState = SSTATE_INIT;
   }

}

unsigned int signalMax;
unsigned int signalMin;
unsigned long startMillis;  // Start of sample window
double peakToPeak;   // peak-to-peak level
void CollectAndSendData()
{
   if (collectState == STATE_INIT)
   {
      n = 0;
      total = 0;
      //Serial.println("D: State_init");

      collectState = STATE_SAMPLE;
   }

   // Collect data for 50 mS
   else if (collectState == STATE_SAMPLE)
   {
      startMillis = millis();
      signalMax = 0;
      signalMin = 1024;
      peakToPeak = 0;
      while (millis() - startMillis < sampleWindow)
      {
         sample = analogRead(A0);
         if (sample <= 1000)  // toss out spurious readings
         {
            if (sample > signalMax)
            {
               signalMax = sample;  // save just the max levels
            }
            else if (sample < signalMin)
            {
               signalMin = sample;  // save just the min levels
            }
         }
      }
      collectState = STATE_PEAK_PEAK;
      //Serial.println("D: Sample Complete");
   }

   else if (collectState == STATE_PEAK_PEAK)
   {
      peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
      peakToPeak = peakToPeak/1000;
      Serial.print(peakToPeak);

      if (peakToPeak > .8)
      {
       total += peakToPeak*50;
       n += 50;
      }
      else if (peakToPeak > .7)
      {
       total += peakToPeak*20;
       n += 30;
      }
      else if (peakToPeak > .6)
      {
       total += peakToPeak*20;
       n += 20;
      }
      else if (peakToPeak > .5)
      {
       total += peakToPeak*16;
       n +=16;
      }
      else if (peakToPeak > .4)
      {
       total +=peakToPeak*10;
       n +=10;
      }
      else if (peakToPeak > .3)
      {
       total +=peakToPeak*8;
       n +=9;
      }
      else if (peakToPeak > .2)
      {
         total +=peakToPeak*6;
         n +=6;
      }
      else if (peakToPeak > .1)
      {
       total +=peakToPeak*1;
       n +=1;
      }
      total += peakToPeak;
      n++;
      running_av = (double)total/n;

      // Serial.print(",");
      if (n > 300)
      {
         collectState = STATE_SEND;
      }
      else
      {
         collectState = STATE_SAMPLE;
         //Serial.print("D: n: ");
         //Serial.println(n);
         Serial.print("\n");
      }

      // Delay to let the wifi module update.
      if (cycleCount > 10)
      {
         delay(100);
         cycleCount = 0;
      }
      cycleCount++;
   }

   else if (collectState == STATE_SEND)
   {
      //Serial.print("D: running_av: ");
      Serial.print(",");
      Serial.println(running_av);
      SendData(running_av);
      collectState = STATE_INIT;
   }

   else
   {
      collectState = STATE_INIT;
   }
   // Serial.print("\n");
}

